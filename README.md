A solver for the NYT spelling-bee puzzle.

```
Usage of spellingbee:
  -dictFilePath string
    	Path to dictionary file (default "/usr/share/dict/words")
  -letters string
    	Letters to form words
  -requiredLetter string
    	Letter which must occur in word
```

