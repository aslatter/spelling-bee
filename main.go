package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

var dictFilePath string
var letters string
var requiredLetter string

func main() {
	flag.StringVar(&dictFilePath, "dictFilePath", "/usr/share/dict/words", "Path to dictionary file")
	flag.StringVar(&letters, "letters", "", "Letters to form words")
	flag.StringVar(&requiredLetter, "requiredLetter", "", "Letter which must occur in word")
	flag.Parse()

	if letters == "" {
		fmt.Fprintln(os.Stderr, "Flag '-letters' is required")
		os.Exit(1)
	}
	if requiredLetter == "" {
		fmt.Fprintln(os.Stderr, "Flag '-requiredLetter' is required")
		os.Exit(1)
	}

	requiredLetters := []rune(requiredLetter)
	if len(requiredLetters) != 1 {
		fmt.Fprintln(os.Stderr, "Flag '-requiredLetter' must specify at most one letter")
		os.Exit(1)
	}

	requiredLetter := requiredLetters[0]
	allowedRunes := []rune(letters)

	dictReader, err := os.Open(dictFilePath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error opening dictionary: ", err)
		os.Exit(1)
	}

	dictLines := bufio.NewScanner(dictReader)
	for dictLines.Scan() {
		candidateWord := dictLines.Text()
		if len(candidateWord) < 4 {
			continue
		}

		var foundRequired bool
		var invalid bool

	candidateLetterLoop:
		for _, c := range candidateWord {
			if c == requiredLetter {
				foundRequired = true
				continue candidateLetterLoop
			}
			// this is a "bad" algorithm, but it's
			// fast enough.
			for _, a := range allowedRunes {
				if a == c {
					// this letter is allowed, move to the next
					continue candidateLetterLoop
				}
			}
			// candidate letter is not allowed
			invalid = true
			break
		}
		invalid = invalid || !foundRequired
		if invalid {
			// try the next word
			continue
		}
		// success! print out the found word
		fmt.Println(candidateWord)
	}
}
